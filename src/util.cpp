#include "util.h"

#include <QtDebug>

namespace mhcp {

QTime qtimeFromMSecs(int msecs)
{
	QTime time;
	return time.addMSecs(msecs);
}

QTime substractTime(QTime t1, QTime t2)
{
	int secs = t2.secsTo(t1);
	QTime result;
	result = result.addSecs(secs);
	return result;
}

QTime addTime(QTime t1, QTime t2)
{
	//qDebug() << "adding " << t2.toString() << " to " << t1.toString();
	return t1.addSecs(t2.hour() * 3600 + t2.minute() * 60 + t2.second());
}


}
