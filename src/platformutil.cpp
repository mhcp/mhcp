#include "platformutil.h"

#include <QtGlobal> // necessary for Q_OS_WIN define

#if defined(Q_OS_WIN)
#include "platformutil_win.cpp"
#elif defined(Q_OS_LINUX)
#include "platformutil_lin.cpp"
#endif
