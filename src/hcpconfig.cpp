#include "hcpconfig.h"

#include <QSettings>
#include <QTime>
#include <QDir>

namespace mhcp {

MhcpConfigDialog::MhcpConfigDialog(QObject* parent)
{
	setupUi(this);

	loadConfig();

	initGUI();
}

MhcpConfigDialog::~MhcpConfigDialog()
{
	saveConfig();
}

void MhcpConfigDialog::initGUI()
{
	connect(okButton, SIGNAL(clicked()), this, SLOT(saveConfig()));

	qDebug() << " workduration = " << workDuration;
	workDurationTE->setTime(workDuration);
	lunchBreakStartTE->setTime(lunchBreakStart);
	lunchBreakStopTE->setTime(lunchBreakStop);
	drinkBreakTE->setTime(drinkBreakFreq);
}

void MhcpConfigDialog::loadConfig()
{
	qDebug() << "reading settings";
	QSettings settings;

	version = MHCP_VER;
	checkVersionURL = settings.value("general/checkVersionURL", "u80235:8080/mhcp/latest_version.txt").toString();
	checkForUpdates = settings.value("general/checkForUpdates", true).toBool();
	dbPath = settings.value("general/dbPath", QDir::homePath()+"/mhcp.db").toString();

	workDuration = settings.value("config/workDuration", QTime(8,36)).toTime();
	lunchBreakStart = settings.value("config/lunchBreakStart", QTime(12,00)).toTime();
	lunchBreakStop = settings.value("config/lunchBreakStop", QTime(13,00)).toTime();
        drinkBreakFreq = settings.value("config/drinkBreakFreq", QTime(0,45)).toTime();
        workHiatusPromptSecs = settings.value("config/workHiatusPromptSecs", 3600).toInt();
        promptUnlessDayShift = settings.value("config/promptUnlessDayShift", true).toBool();

        useBootTime = settings.value("config/useBootTime", true).toBool();
        useBootTimeThreshold = settings.value("config/useBootTimeThreshold", 30*60).toInt();

	drinkNotificationType = settings.value("config/drinkNotificationType", "Balloon").toString();
	leaveNotificationType = settings.value("config/leaveNotificationType", "Popup").toString();
	
	pingInterval = settings.value("config/pingInterval", 5000).toInt();
}

void MhcpConfigDialog::saveConfig()
{
	qDebug() << "writing settings";
	QSettings settings;
	settings.setValue("general/version", MHCP_VER);
	settings.setValue("general/checkVersionURL", checkVersionURL);
	settings.setValue("general/checkForUpdates", checkForUpdates);
	settings.setValue("general/dbPath", dbPath);
	
	settings.setValue("config/workDuration", workDuration);
	settings.setValue("config/lunchBreakStart", lunchBreakStart);
	settings.setValue("config/lunchBreakStop", lunchBreakStop);
	settings.setValue("config/drinkBreakFreq", drinkBreakFreq);
	
	settings.setValue("config/drinkNotificationType", drinkNotificationType);
	settings.setValue("config/leaveNotificationType", leaveNotificationType);

	settings.setValue("config/pingInterval", pingInterval);
	
}

} // namespace
