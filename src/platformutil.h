#ifndef PLATFORMUTIL_H
#define PLATFORMUTIL_H


namespace mhcp {

class PlatformUtil
{
public:
    static long getMilliSecsSinceBoot();
};

} // namespace mhcp

#endif // PLATFORMUTIL_H
