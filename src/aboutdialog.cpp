#include "aboutdialog.h"
#include <QtDebug>

#include "mhcpapp.h"

namespace mhcp {


AboutDialog::AboutDialog()
{
	qDebug() << "creating about dialog";
	setupUi(this);
	initGUI();
}


void AboutDialog::initGUI()
{
	versionLabel->setText(QString("version %1").arg(MHCP_APP_VERSION));
}

}//namespace mhcp