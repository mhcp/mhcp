#ifndef __MHCPAPP_H__
#define __MHCPAPP_H__

#include "hcpconfig.h"
#include <QObject>
#include <QTime>
#include <QSystemTrayIcon>
#include <QSqlDatabase>
#include <QTimer>

class QDateTime;

namespace mhcp {

#define MHCP_APP_VERSION "0.1"

class HistoryDialog;
class AboutDialog;

enum TIME_TYPE { START_TIME, STOP_TIME };

class MhcpApp : public QObject
{
	Q_OBJECT

public:
	MhcpApp(QObject* parent = NULL);
	virtual ~MhcpApp();
	void init();
	void initGui();
	void initDrinkTimer();
	void initLeaveTimer();

	void notify(QString type, QString text);
	QTime getTimeFromDB(QDate date, TIME_TYPE type);
        bool insertTimeIntoDB(QDateTime datetime, TIME_TYPE type, bool forceUpdate = false);
        bool isEnabled();

public slots:
	void onShowAboutDialog();
	void onShowConfig();
	void onShowHistory();
	void onQuit();
	void onTrayIconActivated(QSystemTrayIcon::ActivationReason reason);
	void setMuteNotificationsEnabled(bool mute);
	void onDrinkNotification();
	void onLeaveNotification();

private slots:
	void saveStopTime();
	void onPing();
        void onEnabledAction_toggled(bool checked);

private:
	bool clearStopTime(QDate date);
	void notifyBalloon(QString text);
	void notifyPopup(QString text);

	bool openOrCreateDB();
	void closeDB();
	//bool insertTime(TIME_TYPE type);
	QDateTime getLastStartTime();

	bool notifications;
        HistoryDialog* historyDialog;
        AboutDialog* aboutDialog;
	//QCoreApplication* app;
	QSystemTrayIcon* trayIcon;
	QDateTime startTime;
	MhcpConfigDialog* mhcpConfig;
	QDateTime lastPing;
        QTimer leaveTimer;
	//int rowId;
        QAction* enabledAction;
};

} //namespace mhcp
#endif
