#ifndef __MHCPCONFIG_H__
#define __MHCPCONFIG_H__

#include <QDialog>
#include <QObject>
#include <QtDebug>
#include <QString>
#include <QTime>

#include "ui_configdialog.h"

namespace mhcp {

#define MHCP_VER "0.1"

class MhcpConfigDialog : public QDialog, private Ui::MhcpConfigDialog
{
	Q_OBJECT

public:
	MhcpConfigDialog(QObject* parent = NULL);
	virtual ~MhcpConfigDialog();

	void initGUI();

private slots:
	void saveConfig();

public:
	void loadConfig();

	QString version;
	QString checkVersionURL;
	bool checkForUpdates;
	QString dbPath;

	int	pingInterval;
	QTime workDuration;
	QTime lunchBreakStart;
	QTime lunchBreakStop;
        QTime drinkBreakFreq;
        int workHiatusPromptSecs;
        bool promptUnlessDayShift;

        bool useBootTime;
        int useBootTimeThreshold;

	QString drinkNotificationType;
	QString leaveNotificationType;

};

} //namespace mhcp

#endif
