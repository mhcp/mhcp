#include "platformutil.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

namespace mhcp {

long PlatformUtil::getMilliSecsSinceBoot()
{
    return ::GetTickCount();
}

} //namespace mhcp
