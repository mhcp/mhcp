#ifndef __HISTORYDIALOG_H__
#define __HISTORYDIALOG_H__

#include <QDialog>
#include <QObject>
#include <QtDebug>
#include <QString>

#include "ui_historydialog.h"

#include "mhcpapp.h"

namespace mhcp {

class HistoryDialog : public QDialog, private Ui::HistoryDialog
{
	Q_OBJECT

public:
	HistoryDialog(MhcpApp* app);
	virtual ~HistoryDialog() {};

	void initGUI();

protected:
	virtual void showEvent ( QShowEvent * event );

private slots:
	void onSelectionChanged();
        void onEdit(QDate);

private:
	void updateLabels(QDate date);


	MhcpApp* app;
};



} //namespace mhcp

#endif
